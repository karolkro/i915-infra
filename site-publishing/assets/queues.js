function format_date(raw_date) {
    var diff = Date.now() - (new Date(raw_date+"+00:00"));

    var h = Math.floor(diff / 1000 / 60 / 60);
    diff -= h * 1000 * 60 * 60;
    var m = Math.floor(diff / 1000 / 60);

    var response = ""

    if (h > 0)            { response += h + "h " }
    if (m > 0 || h > 0)   { response += m + "min " }
    if (m == 0 && h == 0) { response += "less than a minute " }

    response += "ago";

    return response;
}

var BASE_URL="https://patchwork.freedesktop.org/api/1.0/";

function get_json(url, handler) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            handler(this.status == 200 ? JSON.parse(this.responseText) : undefined);
        }
    };

    xhttp.open("GET", url, true);
    xhttp.setRequestHeader('Accept', 'application/json');
    xhttp.send();
}

function query_series(series_id) {
    get_json(BASE_URL + 'series/' + series_id + '/',
        function(response) {
            var message = response === undefined ? "Error while fetching " + series_id : response['name']
            document.querySelector('#name'+series_id).innerHTML = message;
        });
}

function only_unique(arr) {
    return arr.filter(function(value, index, self) {
        series_id = value['series']
        ei = self.findIndex(function(e) { return e['series'] == series_id });
        return index == ei;
    });
}


function get_events(name, timestamp) {
    get_json(BASE_URL + 'projects/' + name + "/events/?name=series-new-revision&since=" + timestamp,
        function(response) {
            if (response === undefined){
                document.getElementById(name + "-spinner").outerHTML = "Error while fetching data";
                return;
            }
            results = only_unique(response['results']).reverse();

            var i;

            document.getElementById(name + "-spinner").outerHTML = "";
            if (results.length == 0) {
                document.querySelector('#' + name).innerHTML +=
                    '<tr><td colspan="4">' +
                    "There's nothing in this queue!" +
                    '</td></tr>';
            }
            for (i = 0; i < results.length; ++i) {
                item = results[i];
                series_id = item['series'];
                event_time = item['event_time'];

                document.querySelector('#' + name).innerHTML +=
                    '<tr>' +
                    '<td>' + (i+1) + '</td>' +
                    '<td><a href="https://patchwork.freedesktop.org/series/' + series_id + '/">' +
                     series_id + 'v' + item['parameters']['revision'] +
                    '</a></td>' +
                    '<td id="name' + series_id + '">fetching...</td>' +
                    '<td>' + format_date(event_time) + '</td>' +
                    '</tr>';
                query_series(series_id);
            };
        });
}


function query_timestamp(name, timestamp_file) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                get_events(name, this.responseText);
            } else {
                document.querySelector('#' + name).outerHTML = this.responseText + "<br/>";
                document.getElementById(name + "-spinner").outerHTML = "";
            }
        }
    };

    xhttp.open("GET", timestamp_file + "?r=" + new Date().getTime(), true);
    xhttp.send();
}

function fill_shard_name(series) {
    get_json(BASE_URL + 'series/' + series.id + '/',
        function(response) {
            var message = response === undefined ? "Error while fetching." : response['name'];
            document.querySelector('#shard-name-' + series.name).innerHTML = message;
        });
}

function process_shards(name, shards) {
    document.getElementById(name + "-spinner").outerHTML = "";
    if (shards.length == 0) {
        document.querySelector('#' + name).innerHTML +=
            '<tr><td colspan="4">' +
            "There's nothing in this queue!" +
            '</td></tr>';
    }
    for (var i = 0; i < shards.length; i++) {
        var shard = shards[i];
        var series = shard.series;

        document.querySelector('#' + name).innerHTML +=
            '<tr>' +
              '<td>' + (i+1) + '</td>' +
              '<td>' +
                '<a href="https://patchwork.freedesktop.org/series/' + series.id  + '/#rev' + series.rev  + '">' +
                   series.name +
                '</a>' +
              '</td>' +
              '<td id="shard-name-' +  series.name + '">fetching...</td>' +
              '<td>' + format_date(shard.date) + '</td>' +
            '</tr>';
        fill_shard_name(series);
    }
 
}

function query_shards(name, timestamp_file) {
    var xhttp = new XMLHttpRequest();

    var worker = new Worker("/assets/shards_queue_worker.js");

    worker.onmessage = function(e) {
        var ordered_list = e.data;
        process_shards(name, ordered_list);
    };

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                worker.postMessage(this.responseText);
            } else {
                document.querySelector('#' + name).outerHTML = this.responseText + "<br/>";
                document.getElementById(name + "-spinner").outerHTML = "";
            }
        }
    };

    xhttp.open("GET", timestamp_file + "?r=" + new Date().getTime(), true);
    xhttp.send();
}
